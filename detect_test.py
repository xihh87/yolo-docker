from detect import *

def test_get_labels():
    assert len(get_labels("./coco.names")) == 80

def test_get_colors():
    labels = ["UNO", "DOS", "tres"]
    assert len(get_colors(labels)) == 3

def test_load_model():
    load_model("./yolov3.cfg", "./yolov3.weights")

def test_layer_names():
    net = load_model("./yolov3.cfg", "./yolov3.weights")
    print(layer_names_from(net))

def test_detection():
    blob = np.random.standard_normal([1, 3, 416, 416]).astype(np.float32)
    net = load_model("./yolov3.cfg", "./yolov3.weights")
    detect_objects(blob, net)

def test_read_image():
    image = read_image("./test1.jpg")

def test_get_bounding_boxes():
    image = read_image("./test1.jpg")
    net = load_model("./yolov3.cfg", "./yolov3.weights")
    blob = image_to_blob(image)
    predictions = detect_objects(blob, net)
    boxes = get_bounding_boxes(image, predictions)
    assert type(boxes) == dict

def test_select_best_boxes():
    image = read_image("./test1.jpg")
    net = load_model("./yolov3.cfg", "./yolov3.weights")
    blob = image_to_blob(image)
    predictions = detect_objects(blob, net)
    boxes = get_bounding_boxes(image, predictions)
    idx = select_best_boxes(boxes)

def test_draw_boundning_boxes():
    image = read_image("./test1.jpg")
    blob = image_to_blob(image)
    net = load_model("./yolov3.cfg", "./yolov3.weights")
    predictions = detect_objects(blob, net)
    boxes = get_bounding_boxes(image, predictions)
    img = draw_bounding_boxes(image, boxes)
    assert image.shape == img.shape

def test_write_img():
    image = np.random.standard_normal([216, 216, 3]).astype(np.float32)
    image = ((image - image.min()) * (1/(image.max() - image.min()) * 255)).astype('uint8')
    fname = os.path.realpath("puedo-escribir.jpg")
    dirname = os.path.dirname(fname)
    with open(fname, 'w') as fsock:
        fsock.write('hola')
    print(f"file: {fname} is writable")
    print(image)
    print(image.shape)
    assert save_image(fname, image)
