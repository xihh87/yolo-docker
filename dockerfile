FROM ubuntu:18.04

RUN apt-get update -y \
&& apt-get install -y \
	python3-virtualenv


COPY ./requirements.txt /app/requirements.txt
WORKDIR /app

RUN python3 -m virtualenv --python=/usr/bin/python3 .venv
RUN . .venv/bin/activate \
&& python -m pip install -r requirements.txt

COPY . /app

ENTRYPOINT []

CMD [ "python3", "app.py" ]
