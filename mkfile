help:Q:	## show this message
	cmd/show-help mkfile

start:Q:	## install development environment
	virtualenv .venv
	. .venv/bin/activate
	pip install -r requirements.txt

dl:V:	yolov3.weights	yolov3.cfg	coco.names	## download required files

yolov3.weights:
	curl -LO https://pjreddie.com/media/files/yolov3.weights

yolov3.cfg:
	curl -LO https://raw.githubusercontent.com/pjreddie/darknet/master/cfg/yolov3.cfg

coco.names:
	curl -LO https://raw.githubusercontent.com/pjreddie/darknet/master/data/coco.names
