import io
import base64

from flask import Flask, request, Response, jsonify, render_template
from detect import *

app = Flask(__name__)


def image_to_byte_array(image:Image):
    img = io.BytesIO()
    image.save(img, format='png')
    img = img.getvalue()
    return img


def read_image_from(request):
    img = request.files["image"].read()
    img = Image.open(io.BytesIO(img))

    npimg = np.array(img)
    image = npimg.copy()

    image = cv2.cvtColor(image, cv2.COLOR_BGRA2RGB)
    return image


@app.route('/api/detect', methods=['POST'])
def web_detection_base64():
    if not request.files.get('image'):
        return Response(response="No image sent", status=400)

    image = read_image_from(request)
    detect_coco_objects(image)

    image = cv2.cvtColor(image, cv2.COLOR_BGRA2RGB)
    np_img = Image.fromarray(image)

    img = image_to_byte_array(np_img)
    img_encoded = base64.b64encode(img)

    return Response(response=img_encoded, status=200, mimetype="image/png;base64")


@app.route('/api/test', methods=['POST'])
def web_detection():
    if not request.files.get('image'):
        return Response(response="No image sent", status=400)

    image = read_image_from(request)
    detect_coco_objects(image)

    image = cv2.cvtColor(image, cv2.COLOR_BGRA2RGB)
    np_img = Image.fromarray(image)

    img_encoded = image_to_byte_array(np_img)

    return Response(response=img_encoded, status=200, mimetype="image/png")


@app.route("/", methods=['GET'])
def homepage():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
