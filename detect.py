#!python3
import numpy as np
import argparse
import time
import cv2
import os

from matplotlib import pyplot as plt
from PIL import Image

def get_labels(labels_path):
    """
    Load the COCO class labels our YOLO model was trained on.
    """
    with open(labels_path) as fsock:
        return fsock.read().strip().split("\n")

def get_colors(labels):
    """
    Define a color for each label.
    """
    np.random.seed(42)
    colors = np.random.randint(
        0,
        255,
        size=(len(labels), 3),
        dtype="uint8"
    )
    return colors

def load_model(config, weights):
    """
    Load YOLO object detector trained on COCO dataset (80 classes).
    """
    net = cv2.dnn.readNetFromDarknet(config, weights)
    return net


def layer_names_from(network):
    """
    Determine only the Output layernames that we need from YOLO
    """
    return network.getUnconnectedOutLayersNames()

def read_image(filename):
    image = cv2.imread(filename)
    return image

def image_to_blob(image):
    blob = cv2.dnn.blobFromImage(
        image,
        1 / 255.0,
        (416, 416),
        swapRB=False,
        crop=False
    )
    return blob

def detect_objects(blob:np.ndarray, net):
    net.setInput(blob)

    start = time.time()
    predictions = net.forward(layer_names_from(net))
    end = time.time()

    print(f"[INFO]: YOLO took {(end - start):.4f} seconds")

    return predictions

def extract_scores(prediction):
    return prediction[5:]

def valid_inference(confidence, threshold=0.5):
    return confidence > threshold

def best_score(scores):
    return np.argmax(scores)

def get_bounding_boxes(image:Image, predictions):
    (H, W) = image.shape[:2]
    boxes = []
    classes = []
    scores = []
    for output in predictions:
        for detection in output:
            confidences = extract_scores(detection)
            class_id = best_score(confidences)
            confidence = confidences[class_id]

            if valid_inference(confidence):
                box = detection[0:4] * np.array([W, H, W, H])
                (center_x, center_y, w, h) = box.astype("int")
                x = int(center_x - (w / 2))
                y = int(center_y - (h / 2))

                boxes.append((x, y, w, h))
                classes.append(class_id)
                scores.append(float(confidence))

    return {"regions": boxes, "classes": classes, "scores": scores}

def select_best_boxes(boxes, confidence=0.5, threshold=0.1):
    idx = cv2.dnn.NMSBoxes(
        boxes.get("regions"),
        boxes.get("scores"),
        confidence,
        threshold
    )
    return idx

def draw_bounding_boxes(image, boxes):
    """
    Draw bounding boxes to detected objects.
    """
    labels = get_labels("coco.names")
    colors = get_colors(labels)

    regions = boxes.get("regions")
    classes = boxes.get("classes")
    scores = boxes.get("scores")

    for i in select_best_boxes(boxes):
        (x, y, w, h) = regions[i]
        class_id = classes[i]
        color = [int(c) for c in colors[class_id]]
        start = (x, y)
        end = (x + w, y + h)
        #print(f"Add rectable from {start[0]}, {start[1]}, to {end[0]}, {end[1]}; color: {color}.")
        cv2.rectangle(image, start, end, color, thickness=2)

        label = labels[class_id]
        score = scores[i]
        text = f"{label}: {score:.3f}"
        cv2.putText(
            image,
            text,
            (x, y - 10),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.65,
            color,
            2
        )
    return image

def default_model():
    return load_model("./yolov3.cfg", "./yolov3.weights")

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("image", help="The image you want to detect objects on.")
    parser.add_argument("output", help="Where to save your image.")
    args = parser.parse_args()
    args.output = os.path.realpath(args.output)
    return args

def save_image(filename, image):
    return cv2.imwrite(filename, image)

def detect_coco_objects(image):
    blob = image_to_blob(image)
    net = default_model()
    predictions = detect_objects(blob, net)
    boxes = get_bounding_boxes(image, predictions)
    return draw_bounding_boxes(image, boxes)

def main():
    """
    Draw bounding boxes to detected objects.
    """
    args = parse_args()
    image = read_image(args.image)
    img = detect_coco_objects(image)
    save_image(args.output, img)

if __name__ == "__main__":
    main()
